const fs = require("fs");

function readAFile(filePath, callback) {
  fs.readFile(filePath, "utf-8", (err, data) => {
    callback(err, data);
  });
}

function stringToUpperCase(string, callback) {
  setTimeout(() => {
    try {
      const text = string.toUpperCase();
      callback(undefined, text);
    } catch (err) {
      callback(err);
    }
  }, 0);
}

function stringToLowerCase(stringtext, callback) {
  setTimeout(() => {
    try {
      const text = stringtext.toLowerCase();
      callback(undefined, text);
    } catch (err) {
      callback(err);
    }
  }, 0);
}

function writeAFile(filePath, data, callback) {
  fs.writeFile(filePath, data, (err) => {
    callback(err);
  });
}

function stringSplitText(stringtext, callback) {
  setTimeout(() => {
    try {
      const text = stringtext.split(". ").join("\n");
      callback(undefined, text);
    } catch (err) {
      callback(err);
    }
  }, 0);
}

function appendInFile(filePath, data, callback) {
  fs.appendFile(filePath, "\n" + data, (err) => {
    callback(err);
  });
}

function sortTheContents(textdata, callback) {
  setTimeout(() => {
    try {
      sortedText = textdata.split("\n").sort().join("\n");
      callback(undefined, sortedText);
    } catch (err) {
      callback(err);
    }
  }, 0);
}

function unlinkAfile(file, callback) {
  fs.unlink(file, (err) => {
    callback(err);
  });
}

function problem2(fileName = "lipsum.txt") {
  readAFile(fileName, (err, data) => {
    if (err) {
      console.error(err);
    } else {
      stringToUpperCase(data, (err, upperCaseText) => {
        if (err) {
          console.error(err);
        } else {
          writeAFile("newfile.txt", upperCaseText, (err) => {
            if (err) {
              console.error(err);
            } else {
              writeAFile("filenames.txt", "newfile.txt", (err) => {
                if (err) {
                  console.error(err);
                } else {
                  readAFile("newfile.txt", (err, data) => {
                    if (err) {
                      console.error(err);
                    } else {
                      stringToLowerCase(data, (err, lowerCaseText) => {
                        if (err) {
                          console.error(err);
                        } else {
                          stringSplitText(lowerCaseText, (err, splitedText) => {
                            if (err) {
                              console.error(err);
                            } else {
                              writeAFile("newfile2.txt", splitedText, (err) => {
                                if (err) {
                                  console.error(err);
                                } else {
                                  appendInFile(
                                    "filenames.txt",
                                    "newfile2.txt",
                                    (err) => {
                                      if (err) {
                                        console.error(err);
                                      } else {
                                        readAFile(
                                          "newfile2.txt",
                                          (err, textData) => {
                                            if (err) {
                                              console.error(err);
                                            } else {
                                              sortTheContents(
                                                textData,
                                                (err, sortedText) => {
                                                  if (err) {
                                                    console.error(err);
                                                  } else {
                                                    writeAFile(
                                                      "newfile3.txt",
                                                      sortedText,
                                                      (err) => {
                                                        if (err) {
                                                          console.error(err);
                                                        } else {
                                                          appendInFile(
                                                            "filenames.txt",
                                                            "newfile3.txt",
                                                            (err) => {
                                                              if (err) {
                                                                console.error(
                                                                  err
                                                                );
                                                              } else {
                                                                readAFile(
                                                                  "filenames.txt",
                                                                  (
                                                                    err,
                                                                    filenamesText
                                                                  ) => {
                                                                    if (err) {
                                                                      console.error(
                                                                        err
                                                                      );
                                                                    } else {
                                                                      fileNames =
                                                                        filenamesText.split(
                                                                          "\n"
                                                                        );
                                                                      let completedExecution = 0;
                                                                      for (
                                                                        index = 0;
                                                                        index <
                                                                        fileNames.length;
                                                                        index++
                                                                      ) {
                                                                        unlinkAfile(
                                                                          fileNames[
                                                                            index
                                                                          ],
                                                                          (
                                                                            err
                                                                          ) => {
                                                                            if (
                                                                              err
                                                                            ) {
                                                                              console.error(
                                                                                err
                                                                              );
                                                                            } else {
                                                                              completedExecution += 1;
                                                                              if (
                                                                                completedExecution ===
                                                                                fileNames.length
                                                                              ) {
                                                                                unlinkAfile(
                                                                                  "filenames.txt",
                                                                                  (
                                                                                    err
                                                                                  ) => {
                                                                                    if (
                                                                                      err
                                                                                    ) {
                                                                                      console.error(
                                                                                        err
                                                                                      );
                                                                                    }
                                                                                  }
                                                                                );
                                                                              }
                                                                            }
                                                                          }
                                                                        );
                                                                      }
                                                                    }
                                                                  }
                                                                );
                                                              }
                                                            }
                                                          );
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;
