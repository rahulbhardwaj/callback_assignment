const fs = require("fs");
const path = require("path");

function writeAFile(filePath, data, callback) {
  fs.writeFile(filePath, data, (err) => {
    callback(err);
  });
}

function unlinkAfile(file, callback) {
  fs.unlink(file, (err) => {
    callback(err);
  });
}

function deleteDir(dirPath, callback) {
  fs.rmdir(dirPath, (err) => {
    callback(err);
  });
}

function createDirAndFiles(dirPath, NoOfFiles, callback) {
  fs.mkdir(dirPath, (err) => {
    if (err) {
      callback(err);
    }
    let completedExecution = 0;
    for (index = 0; index < NoOfFiles; index++) {
      const filePath = path.join(dirPath, `files${index}.json`);
      const data = JSON.stringify({ data: Math.random() });
      writeAFile(filePath, data, (err) => {
        if (err) {
          callback(err);
        } else {
          completedExecution += 1;
        }
        if (completedExecution === NoOfFiles) {
          callback();
        }
      });
    }
  });
}

function deleteFiles(dirPath, callback) {
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      callback(err);
    }
    const filesPaths = files.map((file) => path.join(dirPath, file));
    let completedExecution = 0;
    for (let index = 0; index < filesPaths.length; index++) {
      unlinkAfile(filesPaths[index], (err) => {
        if (err) {
          callback(err);
        } else {
          completedExecution += 1;
        }
        if (completedExecution === filesPaths.length) {
          callback();
        }
      });
    }
  });
}

function problem1(
  dirPath = "exampleDIR",
  NoOfFiles = Math.floor(Math.random() * 10)
) {
  createDirAndFiles(dirPath, NoOfFiles, (err) => {
    if (err) {
      console.error(err);
    } else {
      deleteFiles(dirPath, (err) => {
        if (err) {
          console.error(err);
        } else {
          deleteDir(dirPath, (err) => {
            if (err) {
              console.error(err);
            }
          });
        }
      });
    }
  });
}

module.exports = problem1;
